﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Test.ViewModel
{
    public class LoginViewModel
    {
        [Required(ErrorMessage = "Enter user ID")]
        public string UserId { get; set; }

        [Required(ErrorMessage = "Enter user password")]
        public string UserPassword { get; set; }
    }
}