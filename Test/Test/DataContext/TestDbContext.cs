﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Test.Models;

namespace Test.DataContext
{
    public class TestDbContext : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Board> Notes { get; set; }
        
        public TestDbContext() { }
        public TestDbContext(
            DbContextOptions<TestDbContext> options)
            : base(options)
        {
        }

        //db connection.`1
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            //migration setting
            //optionsBuilder.UseSqlServer(@"Server=localhost; Database=TestServer; User Id=sa; Password = sa1234; ");
            optionsBuilder.UseSqlServer("Server=localhost;Database=TestServer;Trusted_Connection=True");
        }
    }
}
