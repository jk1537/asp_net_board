﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Test.DataContext;
using Test.Models;
using Test.ViewModel;

namespace Test.Controllers
{
    public class AccountController : Controller
    {
        /// <summary>
        /// Login
        /// </summary>
        /// <returns></returns>
        [HttpGet]// if not declare then default is [HttpGet]
        public IActionResult Login()
        {
            return View();
        }

        /// <summary>
        /// Login POST
        /// 
        /// if using GET, all information expose to URL.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult Login(LoginViewModel model)
        {
            //ID, Password
            if (ModelState.IsValid) //Check is every textfiled filled?
            {
                using (var db = new TestDbContext())
                {
                    //FirstOrDefault : from user, first or default value.
                    var user = db.Users
                                    .FirstOrDefault(u => u.UserId.Equals(model.UserId)
                                                    && u.UserPassword.Equals(model.UserPassword)); //If lambda statement is true then get first one or defualt one. 
                                                                                                   //u=>u.UserId == model.UserId && u.UserPassword == model.UserPassword);
                                                                                                   //when programmer use '==' then it wll leak memory. 
                                                                                                   // Prevent: Use Equals() object

                    if (user != null) //login success 
                    {
                        //add login session.
                        //HttpContext.Session.SetInt32(key, value);
                        HttpContext.Session.SetInt32("USER_LOGIN_KEY", user.UserNo);

                        return RedirectToAction("LoginSuccess", "Home");
                    }
                }
                //fail login
                ModelState.AddModelError(string.Empty, "UserId or Pwd is not valid"); // null string is string.Empty

            }
            return View(model);
        }

        public IActionResult Logout()
        {
            HttpContext.Session.Remove("USER_LOGIN_KEY");

            return RedirectToAction("Index", "Home");
        }

        /// <summary>
        /// Register
        /// </summary>
        /// <returns></returns>
        // default is [HttpGet]
        public IActionResult Register()
        {
            return View();
        }

        /// <summary>
        /// Register POST
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult Register(User model)
        {
            //validation check
            if (ModelState.IsValid)  // Check whether user input all textfields or not.  [Required] annotation check in User.
            {
                using (var db = new TestDbContext()) // using statement works similar with try-catch in JAVA.
                {
                    db.Users.Add(model);
                    db.SaveChanges(); // save to memory. commit
                }
                return RedirectToAction("Index", "Home"); // send to diffent view.
            } // if not pass if statement 
              //then return to index page.

            return View(model);
        }
    }
}