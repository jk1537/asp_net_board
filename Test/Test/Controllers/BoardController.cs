﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Test.DataContext;
using Test.Models;

namespace Test.Controllers
{
    public class BoardController : Controller
    {
        private readonly TestDbContext _context;

        IHostingEnvironment _env;

        public BoardController(IHostingEnvironment env)
        {
            _env = env;
        }

        [ActivatorUtilitiesConstructor]
        public BoardController(TestDbContext context)
        {
            _context = context;
        }



        // GET: Board
        public async Task<IActionResult> Index(string sortOrder, string currentFilter, string searchString, int? pageNumber)
        {
            //Available board with login only.
            if (HttpContext.Session.GetInt32("USER_LOGIN_KEY") == null)
            {
                return RedirectToAction("Login", "Account");
            }

            ViewData["CurrentSort"] = sortOrder;

            ViewData["NameSortParm"] = String.IsNullOrEmpty(sortOrder) ? "title_desc" : "";
            ViewData["DateSortParm"] = String.IsNullOrEmpty(sortOrder) ? "date_desc" : "";
            ViewData["AuthorSortParm"] = String.IsNullOrEmpty(sortOrder) ? "author_desc" : "";

            ViewData["CurrentFilter"] = searchString;

            //Searching
            if (searchString != null)
            {
                pageNumber = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            var board = from b in _context.Notes
                        select b;
           
            //var UserSessionNo = int.Parse(HttpContext.Session.GetInt32("USER_LOGIN_KEY").ToString());
            
            //ViewBag.GetNoteNo = board.Where(b => b.UserNo == UserSessionNo);

            if (!String.IsNullOrEmpty(searchString))
            {
                board = board.Where(b => b.Title.Contains(searchString)
                                       || b.UserName.Contains(searchString));
            }
            
            //Sorting by Title, Data, and Author
            switch (sortOrder)
            {
                case "title_desc":
                    board = _context.Notes.OrderBy(s => s.Title);
                    break;
                case "date_desc":
                    board = _context.Notes.OrderBy(s => s.Date);
                    break;
                case "author_desc":
                    board = _context.Notes.OrderBy(s => s.UserName);
                    break;
                
            }

            //Paging
            int pageSize = 3;
            return View(await PaginatedList<Board>.CreateAsync(board.AsNoTracking(), pageNumber ?? 1, pageSize));
        }

        // GET: Board/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (HttpContext.Session.GetInt32("USER_LOGIN_KEY") == null)
            {
                return RedirectToAction("Login", "Account");
            }

            if (id == null)
            {
                return NotFound();
            }

            var board = await _context.Notes
                .FirstOrDefaultAsync(m => m.NoteNo == id);
            if (board == null)
            {
                return NotFound();
            }

            return View(board);
        }

        // GET: Board/Create
        public IActionResult Create(IFormFile file)
        {
            if (HttpContext.Session.GetInt32("USER_LOGIN_KEY") == null)
            {
                return RedirectToAction("Login", "Account");
            }

            return View();
        }

        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("NoteNo,Title,Date,UserNo,UserName")] Board board, IFormFile file)
        {
            if (HttpContext.Session.GetInt32("USER_LOGIN_KEY") == null)
            {
                return RedirectToAction("Login", "Account");
            }

            //Attached File handler
            if(file != null && file.Length > 0)
            {
                var imagePath = @"\Upload\Image";
                var uploadPath = _env.WebRootPath + imagePath;

                if (!Directory.Exists(uploadPath))
                {
                    Directory.CreateDirectory(uploadPath);
                }

                var uniqFileName = Guid.NewGuid().ToString();
                var filename = Path.GetFileName(uniqFileName + "." + file.FileName.Split(".")[1].ToLower());
                string fullPath = uploadPath + filename;

                imagePath = imagePath + @"\";
                var filePath = @".." + Path.Combine(imagePath, filename);

                using (var fileStream = new FileStream(fullPath, FileMode.Create))
                {
                    await file.CopyToAsync(fileStream);
                }

                ViewData["FileLocation"] = filePath;
                board.PicPath = filePath;
                return View("../Board/Index");
            }

            //save username and userNo to db
            board.UserNo = int.Parse(HttpContext.Session.GetInt32("USER_LOGIN_KEY").ToString());
            if (board.UserNo > 0)
            {
                using (var db = new TestDbContext())
                {
                    var GetUser = db.Users.FirstOrDefault(u => u.UserNo.Equals(board.UserNo));
                    board.UserName = GetUser.UserName;
                }
            }

            if (ModelState.IsValid)
            {
                _context.Add(board);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(board);
        }

        // GET: Board/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (HttpContext.Session.GetInt32("USER_LOGIN_KEY") == null)
            {
                return RedirectToAction("Login", "Account");
            }

            if (id == null)
            {
                return NotFound();
            }

            var board = await _context.Notes.FindAsync(id);

            //Current user info from current session
            var UserSessionNo = int.Parse(HttpContext.Session.GetInt32("USER_LOGIN_KEY").ToString());
            string getUserName = null;
            if (UserSessionNo > 0)
            {
                using (var db = new TestDbContext())
                {
                    var GetUser = db.Users.FirstOrDefault(u => u.UserNo.Equals(UserSessionNo)); //find user from db using session num
                    getUserName = GetUser.UserName;
                }
            }

            //Don't use Identity, so check admin or users manually.
            if (string.Compare(getUserName, "admin") != 0)
            {
                if (UserSessionNo != board.UserNo) //if current user session number is not same as posted user number, can't modify. 
                {
                    return RedirectToAction("Index", "Board"); //go back to board index page.
                }
            }

            if (board == null)
            {
                return NotFound();
            }
            return View(board);
        }

        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("NoteNo,Title,Date,UserNo,UserName")] Board board)
        {
            if (HttpContext.Session.GetInt32("USER_LOGIN_KEY") == null)
            {
                return RedirectToAction("Login", "Account");
            }

            if (id != board.NoteNo)
            {
                return NotFound();
            }

            board.UserNo = int.Parse(HttpContext.Session.GetInt32("USER_LOGIN_KEY").ToString()); // If admin modify the board, then the board author will be changed to admin.
            if (board.UserNo > 0)
            {
                using (var db = new TestDbContext())
                {
                    var GetUser = db.Users.FirstOrDefault(u => u.UserNo.Equals(board.UserNo));
                    board.UserName = GetUser.UserName;
                }
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(board);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!BoardExists(board.NoteNo))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(board);
        }

        [HttpPost]
        public async Task<IActionResult> DeleteCheckBox()
        {
            foreach(var d in _context.Notes)
            {
                if (d.IsChecked)
                {
                    _context.Notes.Remove(d);
                    await _context.SaveChangesAsync();
                }
            }
            return RedirectToAction(nameof(Index));
        }

            // GET: Board/Delete/5
            public async Task<IActionResult> Delete(int? id)
        {
            if (HttpContext.Session.GetInt32("USER_LOGIN_KEY") == null)
            {
                return RedirectToAction("Login", "Account");
            }

            if (id == null)
            {
                return NotFound();
            }

            //using (var db = new TestDbContext())
            //{
            //   foreach(var d in db.Notes)
            //    {
            //        if (d.IsChecked)
            //        {
            //            db.Notes.Remove(d);
            //            await _context.SaveChangesAsync();
            //            return RedirectToAction(nameof(Index));
            //        }
            //    }
            //}

            var board = await _context.Notes
                .FirstOrDefaultAsync(m => m.NoteNo == id);

            var UserSessionNo = int.Parse(HttpContext.Session.GetInt32("USER_LOGIN_KEY").ToString()); // Permission processing same as Edit
            string getUserName = null;
            if (UserSessionNo > 0)
            {
                using (var db = new TestDbContext())
                {
                    var GetUser = db.Users.FirstOrDefault(u => u.UserNo.Equals(UserSessionNo));
                    getUserName = GetUser.UserName;
                }
            }

            if (string.Compare(getUserName, "admin") != 0)
            {
                if (UserSessionNo != board.UserNo)
                {
                    return RedirectToAction("Index", "Board");
                }
            }
            

            if (board == null)
            {
                return NotFound();
            }

            return View(board);
        }

        // POST: Board/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var board = await _context.Notes.FindAsync(id);
            //var UserSessionNo = int.Parse(HttpContext.Session.GetInt32("USER_LOGIN_KEY").ToString());
            //if(board.IsChecked == true)
            //{
            //    _context.Notes.Remove(board);
            //    await _context.SaveChangesAsync();

            //    return RedirectToAction("Delete","Board");
            //}
            //if (UserSessionNo != board.UserNo)
            //{
            //    return RedirectToAction("Index", "Board");
            //}
            
            _context.Notes.Remove(board);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool BoardExists(int id)
        {
            return _context.Notes.Any(e => e.NoteNo == id);
        }
    }
}
