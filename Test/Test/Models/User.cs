﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Test.Models
{
    public class User
    {
        [Key]  // PK 
        public int UserNo { get; set; }

        /// <summary>
        /// User Name
        /// </summary>
        [Required(ErrorMessage = "Enter User Name")] // Not null
        public string UserName { get; set; }

        /// <summary>
        /// User ID 
        /// </summary>
        [Required(ErrorMessage = "Enter User ID")]
        public string UserId { get; set; }

        /// <summary>
        /// USer Password
        /// </summary>
        [Required(ErrorMessage = "Enter Password")]
        public string UserPassword { get; set; }
    }
}
