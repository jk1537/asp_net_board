﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Test.Models
{
    public class Board
    {
        [Key]
        public int NoteNo { get; set; }

        /// <summary>
        /// Board title
        /// </summary>
        [Required]
        public string Title { get; set; }

        /// <summary>
        /// Board date
        /// </summary>
        [Required]
        public int Date { get; set; }

        /// <summary>
        /// Board user number - FK
        /// </summary>
        public int UserNo { get; set; }

        public string UserName { get; set; }

        public string PicPath { get; set; }

        public bool IsChecked { get; set; } // 이거 때문임 테이블 다 비우고 새로 해야함
    }
}
